<?php

/**
 * Implements hook_token_info_alter().
 */
function commerce_shipping_lite_token_info_alter(&$data)
{
  $data['tokens']['commerce-order']['shipping-lite'] = array(
    'name' => t('Shipping Lite product'),
    'description' => t('The first shipping product on order (if any)'),
    'type' => 'commerce-product',
  );
}

/**
 * Implements hook_tokens_alter().
 */
function commerce_shipping_lite_tokens_alter(array &$replacements, array $context)
{
  if ($context['type'] == 'commerce-order' && !empty($context['data']['commerce_order'])) {
    if ($shipping_lite_tokens = token_find_with_prefix($context['tokens'], 'shipping-lite')) {
      $order = $context['data']['commerce_order'];
      $shipping_pid = commerce_shipping_lite_pid($order);
      if ($shipping_pid && $shipping_product = commerce_product_load($shipping_pid)) {
        $replacements += token_generate('commerce-product', $shipping_lite_tokens,
          array('commerce_product' => $shipping_product), $context['options']);
      }
    }
  }
}